# coding: utf-8
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('viaz.urls' )),
    url(r'^', include('authorization.urls' )),
    url(r'^', include('crud.urls' )),
    url(r'^robots\.txt$', TemplateView.as_view( template_name='robots.txt', content_type='text/plain' )),
    url(r'^sitemap\.xml$', TemplateView.as_view(template_name='sitemap.xml', content_type='text/xml')),
]

