# -*- coding: utf-8 -*-
#!/usr/bin/python --2.7
# forms.py
from django import forms
from django.forms import ModelForm, Textarea, IntegerField

from django.forms import ModelForm, ModelMultipleChoiceField
from django.forms.models import inlineformset_factory
from django.contrib.admin.widgets import FilteredSelectMultiple

from .models import * # Change as necessary


class ChapterForm(ModelForm):
    class Meta:
        model = Chapter
        fields = '__all__'

class SectionForm(ModelForm):
    class Meta:
        model = Section
        exclude = ['chapter']

class SubsectionForm(ModelForm):
    class Meta:
        model = Subsection
        # fields = ['name', 'order_num', 'chapter', 'section']
        fields = '__all__'

class PartialSubsectionForm(ModelForm):
    class Meta:
        model = Subsection
        exclude = ['chapter', 'section']

class ElementForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(ElementForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['row', 'column', 'positions']

    class Meta:
        model = Element
        exclude = ['chapter', 'section']
        # fields = ['el_type', 'text', 'row', 'column', 'position', 'image_num', 'image']

ElementFormSet = inlineformset_factory(Subsection, Element, extra=2, fields = ['el_type', 'text', 'row', 'column', 'position', 'im_type'])



class RightColElementForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(ElementForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['row', 'column', 'positions']

    class Meta:
        model = RightColElement
        fields = ['el_type', 'text', 'adcode', 'link', 'position', 'is_txt_img', 'image']


