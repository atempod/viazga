# -*- coding: utf-8 -*-
#!/usr/bin/python --2.7
from __future__ import unicode_literals
from model_utils import Choices
from datetime import datetime

from django.db import models
from django.core.urlresolvers import reverse


class Chapter(models.Model):
    name = models.CharField(u'название**', max_length=128, unique=True)
    # slug = models.SlugField('Slug')
    order_num = models.PositiveIntegerField(u'порядковый номер', unique=True)
    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField(u'дата последнего изменения',
                                    auto_now_add = False, auto_now=True)

    def __unicode__(self):
        return (u'%s' % self.name)

    def __str__(self):
        return (u'%s' % self.name)

    class Meta:
        verbose_name = u"Глава"
        verbose_name_plural = u"Главы"
        ordering = ('order_num',)

    def get_absolute_url(self):
        return reverse('man_contents')


class SectionManager(models.Manager):
    """ Draft/published selection """
    def drafted(self):
        return self.filter(is_draft=True)
    def published(self):
        return self.filter(is_draft=False)


class Section(models.Model):
    name = models.CharField(u'название**', max_length=128, unique=True)
    # slug = models.SlugField('Slug')
    order_num = models.PositiveIntegerField(u'порядковый номер', default=0)
    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    is_draft = models.BooleanField(u'черновик', default=True)  
    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    first_published_at = models.DateTimeField(u'дата опубликования', 
                                    auto_now_add=True, auto_now=False, 
                                    null=True, blank=True)
    updated_at = models.DateTimeField(u'дата последнего изменения',
                                    auto_now_add = False, auto_now=True)

    chapter = models.ForeignKey(Chapter, related_name='section', 
                                    on_delete=models.CASCADE)
    objects = SectionManager()

    def __unicode__(self):
        return u"{0}, {1}".format(self.chapter, self.name)
        # return (u'%s' % self.name)

    def __str__(self):
        return (u'%s' % self.name)

    class Meta:
        verbose_name = u"Раздел"
        verbose_name_plural = u"Разделы"
        ordering = ('order_num',)

    def get_absolute_url(self):
        return reverse('man_contents')


class Subsection(models.Model):
    name = models.CharField(u'название', max_length=128, unique=True)
    order_num = models.PositiveIntegerField(u'порядковый номер', default=0)
    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField(u'дата последнего изменения',
                                    auto_now_add = False, auto_now=True)

    chapter = models.ForeignKey(Chapter, related_name='subsection', 
                                    on_delete=models.CASCADE)
    section = models.ForeignKey(Section, related_name='subsection', 
                                    on_delete=models.CASCADE)

    def __unicode__(self):
        return (u'%s' % self.name)

    def __str__(self):
        return (u'%s' % self.name)

    class Meta:
        verbose_name = u"Подраздел"
        verbose_name_plural = u"Подразделы"
        ordering = ('order_num',)

    def get_absolute_url(self):
        # return reverse('subsec_edit', kwargs={'subsection_id':self.id})
        return reverse('subsection_edit', kwargs={'subsection_id':self.id})


class Element(models.Model):
    ELTYPE = (
        ('PARA', 'Текст'),
        ('HINT', 'Совет'),
        ('FIGU', 'Рисунок'),
    )

    IMTYPE = (
        ('NARR', 'Узкий'),
        ('WIDE', 'Широкий'),
    )

    get_file_path='images'

    el_type = models.CharField(u'тип эл-та', max_length=4, choices=ELTYPE, 
                                    default='PARA')
    text = models.TextField(u'текст', null=True, blank=True)
    row = models.IntegerField(u'ряд', default=0, null=True, blank=True) ## element row number
    column = models.IntegerField(u'столбец', default=0, null=True, blank=True) ## element column number
    position = models.IntegerField(u'позиция', default=0, null=True, blank=True) ## element position in_cell number
    image = models.ImageField(verbose_name=u'Рисунок',upload_to=get_file_path, 
                                    max_length=256, blank=True, null=True)
    image_num = models.IntegerField(u'номер рис.', default=0, 
                                    null=True, blank=True)
    im_type = models.CharField(u'тип рис.', max_length=4, choices=IMTYPE, 
                                    default='NARR')

    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField(u'дата последнего изменения',
                                    auto_now_add = False, auto_now=True)

    chapter = models.ForeignKey(Chapter, related_name='element', 
                                    on_delete=models.CASCADE)
    section = models.ForeignKey(Section, related_name='element', 
                                    on_delete=models.CASCADE)
    subsection = models.ForeignKey(Subsection, related_name='element', 
                                    on_delete=models.CASCADE)

    def __unicode__(self):
        return (u'%s' % self.text)

    def __str__(self):
        return (u'%s' % self.text)

    class Meta:
        verbose_name = u"Элемент"
        verbose_name_plural = u"Элементы"
        ordering = ('row', 'column', 'position',)



class RightColElement(models.Model):
    RELTYPE = (
        ('ADV1', 'Рекл_img'),
        ('ADV2', 'Рекл_txt'),
        ('TXT1', 'Текст1'),
        ('TXT2', 'Текст2'),
        ('IMG1', 'Рис1'),
        ('IMG2', 'Рис2'),
    )

    get_file_path='images'

    el_type = models.CharField(u'тип эл-та', max_length=4, choices=RELTYPE, 
                                    default='IMG1')
    text = models.TextField(u'текст', null=True, blank=True)
    adcode = models.TextField(u'код рекламы', null=True, blank=True)
    link = models.URLField(u'ссылка', null=True, blank=True)
    # to_page = models.TextField(u'стр_на_сайте', null=True, blank=True)
    to_page = models.TextField(u'на страницу', null=True, blank=True)
    position = models.IntegerField(u'позиция', default=0, 
                                    null=True, blank=True) ## element position in_cell number
    is_txt_img = models.ImageField(verbose_name=u'вкл_txt',
                                    upload_to=get_file_path, max_length=256, 
                                    blank=True, null=True)
    image = models.ImageField(verbose_name=u'рисунок',upload_to=get_file_path, 
                                    max_length=256, blank=True, null=True)
    created_at = models.DateTimeField(u'дата создания', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField(u'дата последнего изменения',
                                    auto_now_add = False, auto_now=True)


    def __unicode__(self):
        return (u'%s' % self.text)

    def __str__(self):
        return (u'%s' % self.text)

    class Meta:
        verbose_name = u"РЭлемент"
        verbose_name_plural = u"РЭлементы"
        ordering = ('to_page', 'position',)


class Index(models.Model):
    text = models.TextField(u'текст', null=True, blank=True)
    chapter = models.ForeignKey(Chapter, related_name='indexpage', 
                                    on_delete=models.CASCADE)
    section = models.ForeignKey(Section, related_name='indexpage', 
                                    on_delete=models.CASCADE)
    subsection = models.ForeignKey(Subsection, related_name='indexpage', 
                                    on_delete=models.CASCADE)

    def __unicode__(self):
        return (u'%s' % self.text)

    def __str__(self):
        return (u'%s' % self.text)

    class Meta:
        verbose_name = u"Ключевое слово"
        verbose_name_plural = u"Ключевые слова"
        ordering = ('text',)




