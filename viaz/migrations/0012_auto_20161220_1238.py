# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viaz', '0011_auto_20161211_1533'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subsection',
            name='name',
            field=models.CharField(unique=True, max_length=128, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
    ]
