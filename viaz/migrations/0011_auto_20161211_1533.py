# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viaz', '0010_auto_20161205_1307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='element',
            name='el_type',
            field=models.CharField(default='PARA', max_length=4, choices=[('SUBT', 'Subsection Title'), ('PARA', 'Paragraph Text'), ('HINT', 'Hint'), ('NOTE', 'Note for Figure'), ('FIGU', 'Figure')]),
        ),
    ]
