# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viaz', '0007_auto_20161125_1731'),
    ]

    operations = [
        migrations.CreateModel(
            name='Element',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('el_type', models.CharField(default='PARA', max_length=1, choices=[('T', 'Title of Subsection'), ('P', 'Text of Paragraph'), ('H', 'Hint'), ('M', 'Mark out'), ('F', 'Figure')])),
                ('text', models.TextField(null=True, blank=True)),
                ('position_num', models.IntegerField(default=0)),
                ('image', models.ImageField(max_length=256, upload_to='images', null=True, verbose_name='Image', blank=True)),
                ('element_num', models.IntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f', null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='\u0434\u0430\u0442\u0430 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0433\u043e \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('chapter', models.ForeignKey(related_name='paragraphs', to='viaz.Chapter')),
                ('section', models.ForeignKey(related_name='paragraphs', to='viaz.Section')),
                ('subsection', models.ForeignKey(related_name='paragraphs', to='viaz.Subsection')),
            ],
            options={
                'ordering': ('position_num',),
                'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442',
                'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b',
            },
        ),
        migrations.RemoveField(
            model_name='subhint',
            name='chapter',
        ),
        migrations.RemoveField(
            model_name='subhint',
            name='section',
        ),
        migrations.RemoveField(
            model_name='subhint',
            name='subsection',
        ),
        migrations.RemoveField(
            model_name='subpara',
            name='chapter',
        ),
        migrations.RemoveField(
            model_name='subpara',
            name='section',
        ),
        migrations.RemoveField(
            model_name='subpara',
            name='subsection',
        ),
        migrations.DeleteModel(
            name='SubHint',
        ),
        migrations.DeleteModel(
            name='SubPara',
        ),
    ]
