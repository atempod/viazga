# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-01 16:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viaz', '0012_auto_20161220_1238'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='element',
            options={'ordering': ('row', 'column', 'position'), 'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442', 'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b'},
        ),
        migrations.RemoveField(
            model_name='element',
            name='position_num',
        ),
        migrations.AddField(
            model_name='element',
            name='column',
            field=models.IntegerField(default=0, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0441\u0442\u043e\u043b\u0431\u0446\u0430'),
        ),
        migrations.AddField(
            model_name='element',
            name='position',
            field=models.IntegerField(default=0, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u043f\u043e\u0437\u0438\u0446\u0438\u0438 \u0432 \u044f\u0447\u0435\u0439\u043a\u0435'),
        ),
        migrations.AddField(
            model_name='element',
            name='row',
            field=models.IntegerField(default=0, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0440\u044f\u0434\u0430'),
        ),
    ]
