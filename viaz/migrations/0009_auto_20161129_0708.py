# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viaz', '0008_auto_20161127_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='element',
            name='chapter',
            field=models.ForeignKey(related_name='element', to='viaz.Chapter'),
        ),
        migrations.AlterField(
            model_name='element',
            name='el_type',
            field=models.CharField(default='PARA', max_length=1, choices=[('SUBT', 'Subsection Title'), ('PARA', 'Paragraph Text'), ('HINT', 'Hint'), ('NOTE', 'Note for Figure'), ('FIGU', 'Figure')]),
        ),
        migrations.AlterField(
            model_name='element',
            name='section',
            field=models.ForeignKey(related_name='element', to='viaz.Section'),
        ),
        migrations.AlterField(
            model_name='element',
            name='subsection',
            field=models.ForeignKey(related_name='element', to='viaz.Subsection'),
        ),
        migrations.AlterField(
            model_name='section',
            name='chapter',
            field=models.ForeignKey(related_name='section', to='viaz.Chapter'),
        ),
        migrations.AlterField(
            model_name='subsection',
            name='chapter',
            field=models.ForeignKey(related_name='subsection', to='viaz.Chapter'),
        ),
        migrations.AlterField(
            model_name='subsection',
            name='section',
            field=models.ForeignKey(related_name='subsection', to='viaz.Section'),
        ),
    ]
