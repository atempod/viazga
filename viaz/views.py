#!/usr/bin/python --2.7
# -*- coding: utf-8 -*-
# viaz/views.py
from django.shortcuts import render_to_response, render, get_object_or_404
from django.views.decorators import csrf
from django.template import RequestContext # For CSRF
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import HttpResponse, HttpResponseRedirect

from django.http import HttpResponseRedirect
from django.views.generic import CreateView, UpdateView
from django.views.generic import ListView, TemplateView

from django.core.urlresolvers import reverse 
from django.shortcuts import redirect 

from django.forms.models import modelformset_factory, inlineformset_factory
from django.contrib import messages
from django.views.generic.edit import FormView
from django.db.models import Max

from django.db.models import Q
import re


from .models import *
from .forms import *
 

def indexpage(request):
    ''' View for IndexPage page
    '''
    chapters = Chapter.objects.all().order_by('order_num')
    sections = Section.objects.all().order_by('order_num')
    subsections = Subsection.objects.all().order_by('order_num')

    # return render(request, 'under_construction.html',  {
    return render(request,  'indexpage.html', {
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'indexpage',
                            'title': 'Указатель'
                        })



# def index_page(request):
#     ''' View for manual content  
#     '''    

#     return render(request,  'index_page.html', {
#                             'chapters': chapters,
#                             'sections': sections,
#                             'subsections': subsections,
#                             'is_editor': request.user.is_active and
#                                          request.user.is_staff,
#                             'active_tab': 'manual',
#                         })






def index(request):
    """ Start the initial page of the catalog application """
    chapters = Chapter.objects.all().order_by('order_num')
    sections = Section.objects.all().order_by('order_num')
    subsections = Subsection.objects.all().order_by('order_num')

    return render(request, 'index.html',  {
                            'chapters': chapters,
                            'sections': sections,
                            'subsections': subsections,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'home',
                        })


def user_page(request):
    ''' Personal information page of the current user  
    '''
    return render(request, 'user_page.html',  {
                            'user': request.user,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            # 'user': user,
                        })


def collection(request):
    ''' View for Collection page
    '''
    return render(request, 'under_construction.html',  {
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'collection',
                            'title': 'Коллекции'
                        })
 
def notes(request):
    ''' View for Notes page
    '''
    return render(request, 'under_construction.html',  {
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'notes',
                            'title': 'Заметки'
                        })


def questions(request):
    ''' View for Questions and Answers page
    '''
    return render(request, 'under_construction.html',  {
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'questions',
                            'title': 'Вопросы и ответы'
                        })

def notes(request):
    ''' View for Notes page
    '''
    return render(request, 'under_construction.html',  {
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'notes',
                            'title': 'Заметки'
                        })


# def under_construction(request):
#     ''' Dummy view for pages which are under construction
#     '''
#     return render(request, 'under_construction.html',  {
#                             'is_editor': request.user.is_active and
#                                          request.user.is_staff,
#                             'active_tab': 'collection',
#                         })



def manual_content(request):
    ''' View for manual content  
    '''    
    chapters = Chapter.objects.all().order_by('order_num')
    sections = Section.objects.all().order_by('order_num')
    subsections = Subsection.objects.all().order_by('order_num')

    return render(request,  'manual_content.html', {
                            'chapters': chapters,
                            'sections': sections,
                            'subsections': subsections,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'manual',
                        })



def subsection_view(request, subsection_id):
    ''' View for Subsection object  
    '''
    subsection = Subsection.objects.get(pk=subsection_id)
    chapter = subsection.chapter
    section = subsection.section
    subsections = Subsection.objects.filter(section=section).all()  
    
    elements_list = []
    max_row = Element.objects.all().aggregate(Max('row'))['row__max']
    print('max_row=', max_row)
    if max_row is not None:                     # Check of elements existance
        for i in range(1, max_row + 1):
            for j in range(1,3):
                elements_list.append(Element.objects.filter(subsection=subsection,
                                                        row=i, column=j).all())

    return render(request,  'subsection.html', {
                            'subsection': subsection,
                            'subsections': subsections,
                            'chapter': subsection.chapter,
                            'section': subsection.section,
                            'elements_list': elements_list,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
                            'active_tab': 'manual',
                        })




#==================  search  ====================

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    """ Normalization for searching query """
    return [normspace(' ', (t[0] or t[1]).strip())
                                for t in findterms(query_string)]

def get_query(query_string, search_fields):
    """ Auxiliary function for searching function to form a query """
    query = None # Query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query

def search(request):
    """ Search serial by title and director categories """
    query_string = ''
    found_element_list = []
    found_entries = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        # entry_query = get_query(query_string, ['text', 'chapter',])
        entry_query = get_query(query_string, ['text'])
        # found_entries = Element.objects.filter(entry_query).order_by('-created_at')
        found_entries = Element.objects.filter(entry_query)
        
        for element in found_entries:
            element_id = element.id
            found_element_list.append((element, element_id,))

            print ('fe = ', element.text, element_id)



    return render(request, 'search_results.html', {
                            'is_editor': request.user.is_active and 
                                         request.user.is_staff,
                            
                            'query_string': query_string,
                            'found_element_list': found_element_list,
                            'found_entries': found_entries,
                            })

#==================    ====================
