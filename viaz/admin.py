from django.contrib import admin
from django import forms
from django.contrib.admin import TabularInline, StackedInline, site
from super_inlines.admin import SuperInlineModelAdmin, SuperModelAdmin

# from viaz.models import Chapter, Section, Subsection, SubPara, SubHint
from viaz.models import Chapter, Section, Subsection, Element

class ChapterModelAdmin(SuperModelAdmin):
    list_display = ["name"]

    # class Meta:
    #     model = Chapter


class SectionModelAdmin(SuperModelAdmin):
    list_display = ["name"]

    # class Meta:
    #     model = Section



class SubsectionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SubsectionForm, self).__init__(*args, **kwargs)
        self.fields['section'].queryset = Section.objects.all()\
            .select_related('chapter')

    class Meta:
        model = Subsection
        exclude = ('created_at',) 

class ElementForm(forms.ModelForm):

    # def __init__(self, *args, **kwargs):
    #     super(ElementForm, self).__init__(*args, **kwargs)
    #     self.fields['subsection'].queryset = Subsection.objects.all()\
    #         .select_related('chapter')

    class Meta:
        model = Element
        exclude = ('created_at',) 



# class SubParaModelAdmin(admin.ModelAdmin):
# class SubParaInline(SuperInlineModelAdmin, TabularInline):
#     list_display = ["text"]
#     model = SubPara

#     class Meta:
#         model = SubPara
#         fields = '__all__'


# class SubHintInline(SuperInlineModelAdmin, TabularInline):
#     list_display = ["text"]
#     model = SubHint

#     class Meta:
#         model = SubHint
#         fields = '__all__'

class ElementInline(SuperInlineModelAdmin, TabularInline):
    list_display = ["text"]
    model = Element

    class Meta:
        model = Element
        fields = '__all__'


class SubsectionModelAdmin(SuperModelAdmin):
    list_display = 'name', 'section'
    inlines = [ ElementInline]
    # raw_id_fields = 'section',

    # form = SubsectionForm

class ElementModelAdmin(SuperModelAdmin):
    list_display =  'id', 'el_type', 'text', 'image', 'section' 
    # inlines = [ ElementInline]
    # raw_id_fields = 'section',

    # form = SubsectionForm


admin.site.register(Chapter, ChapterModelAdmin)
admin.site.register(Section, SectionModelAdmin)
admin.site.register(Subsection, SubsectionModelAdmin)
admin.site.register(Element, ElementModelAdmin)
# admin.site.register(SubPara, SubParaInline)
# admin.site.register(Picture)



