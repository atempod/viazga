#!/usr/bin/python --2.7
# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^index/$', views.index, name='index'),
    url(r'^home/$', views.index, name='index'),
    url(r'^manual_content/$', views.manual_content, name='manual_content'),
    url(r'^collection/$', views.collection, name='collection'),
    url(r'^indexpage/$', views.indexpage, name='indexpage'),
    url(r'^questions/$', views.questions, name='questions'),
    url(r'^notes$',  views.notes, name='notes'),
    url(r'^user_page$',  views.user_page, name='user_page'),
    url(r'^subsection/(?P<subsection_id>\d+)/$',  views.subsection_view, name='subsection_view'),
    url(r'^search/$', views.search, name='search'),
    # url(r'^index_page/$', views.index_page, name='index_page'),

    # ======= HELPtools and temporary =======
    url(r'^success$', TemplateView.as_view(template_name='thanks.html'), name='success'),
    # url(r'^under_construction$',  views.under_construction, name='under_construction'),

]




