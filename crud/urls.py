#!/usr/bin/python --2.7
# -*- coding: utf-8 -*-
"""vipro URL Configuration
"""
# crud/urls.py
from django.conf.urls import url
from django.views.generic import TemplateView
from . import views, utils
# from . import views, utils

urlpatterns = [
    url(r'^foreingkey_integrity_check$', utils.foreingkey_integrity_check, name='foreingkey_integrity_check'),
    url(r'^man_contents$', views.man_contents, name='man_contents'),


    # ======= FBV CRUD -- add/update/delete =======
    ## ======= FBV ADD
    url(r'^subsection_add/(?P<section_id>\d+)/$',  views.subsection_add, name='subsection_add'),
    url(r'^chapter_add/$',  views.chapter_add, name='chapter_add'),
    url(r'^section_add/(?P<chapter_id>\d+)/$',  views.section_add, name='section_add'),

    ## ======= FBV EDIT
    url(r'^chapter_edit/(?P<chapter_id>\d+)/$',  views.chapter_edit, name='chapter_edit'),
    url(r'^section_edit/(?P<section_id>\d+)/$',  views.section_edit, name='section_edit'),
    url(r'^subsection_edit/(?P<subsection_id>\d+)/$',  views.subsection_edit, name='subsection_edit'),
    url(r'^right_group_creedit/$',  views.right_group_creedit, name='right_group_creedit'),

    ## ======= FBV DELETE
    url(r'^subsection_delete/(?P<subsection_id>[0-9]*)/$', views.subsection_delete, name='subsection_delete'),
    url(r'^chapter_delete/(?P<chapter_id>[0-9]*)/$', views.chapter_delete, name='chapter_delete'),
    url(r'^section_delete/(?P<section_id>[0-9]*)/$', views.section_delete, name='section_delete'),
    url(r'^element_delete/(?P<element_id>[0-9]*)/$', views.element_delete, name='element_delete'),

]
