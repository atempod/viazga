#!/usr/bin/python --2.7
# -*- coding: utf-8 -*-
# crud/utils.py
from django.shortcuts import render
from django.contrib.admin.views.decorators import staff_member_required
from viaz.models import *


def compare_sets_of_fk_and_id(fk_name, fk_set, id_set):
    """ Compare the sets and return broken id's 
    """    
    diff = fk_set - id_set
    if len(diff):
        print('IntegrityError found for', fk_name, ';', 'broken ForeingKeys:; ', diff)
    else: 
        print(fk_name, ' is OK')
    return diff

@staff_member_required
def foreingkey_integrity_check(request):
    """ Check database on IntegrityError 
        (existance of ForeignKey without corresponding value in parent table) 
        Report the error message, if ForingKey has a broken link to ID value 
    """
    chapters = Chapter.objects.all().order_by('order_num')
    sections = Section.objects.all().order_by('order_num')
    subsections = Subsection.objects.all().order_by('order_num')
    elements = Element.objects.all().order_by('text')

    # Sets of id for every table
    chapter_id_set = set([chapter.id for chapter in chapters])
    section_id_set = set([section.id for section in sections])
    subsection_id_set = set([subsection.id for subsection in subsections])

    # List of subsection_id (as ForeignKey) in the Element table objects
    chapter_fk_from_section_set = set([section.chapter_id for section in sections])
    chapter_fk_from_subsection_set = set([subsection.chapter_id for subsection in subsections])
    chapter_fk_from_element_set = set([element.chapter_id for element in elements])

    section_fk_from_subsection_set = set([subsection.section_id for subsection in subsections])
    section_fk_from_element_set = set([element.section_id for element in elements])

    subsection_fk_from_element_set = set([element.subsection_id for element in elements])
    
    # print('test begins')
    
    compare_sets_of_fk_and_id('subsection_fk_from_element_set', subsection_fk_from_element_set, subsection_id_set)
    compare_sets_of_fk_and_id('section_fk_from_element_set', section_fk_from_element_set, section_id_set)
    compare_sets_of_fk_and_id('section_fk_from_subsection_set', section_fk_from_subsection_set, section_id_set)
    compare_sets_of_fk_and_id('chapter_fk_from_element_set', chapter_fk_from_element_set, chapter_id_set)
    compare_sets_of_fk_and_id('chapter_fk_from_subsection_set', chapter_fk_from_subsection_set, chapter_id_set)
    compare_sets_of_fk_and_id('chapter_fk_from_section_set', chapter_fk_from_section_set, chapter_id_set)


    return render(request, 'man_contents.html',  {
                            'chapters': chapters,
                            'sections': sections,
                            'subsections': subsections,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
        })







