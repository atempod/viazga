#!/usr/bin/python --2.7
# -*- coding: utf-8 -*-
# crud/views.py
import os
from django.shortcuts import render_to_response, render, get_object_or_404
from django.views.decorators import csrf
from django.template import RequestContext # For CSRF
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ObjectDoesNotExist

from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import inlineformset_factory
from django.forms import Textarea
# from django.forms import ModelForm, Textarea, IntegerField

from puerto import settings
from viaz.models import *
from viaz.forms import *
from .forms import *



@staff_member_required
def man_contents(request):
    """ Manual catalog for administration
        Link to add, Edit and Delete of any object of catalog
    """
    chapters = Chapter.objects.all().order_by('order_num')
    sections = Section.objects.all().order_by('order_num')
    subsections = Subsection.objects.all().order_by('order_num')

    return render(request, 'man_contents.html',  {
                            'chapters': chapters,
                            'sections': sections,
                            'subsections': subsections,
                            'is_editor': request.user.is_active and
                                         request.user.is_staff,
        })



# # =========== DELETE CHAPTER  =======================
@staff_member_required
def chapter_delete(request, chapter_id):
    """ Delete chapter instance
        Delete all appropriate section, subsection and element instances
    """
    try:
        chapter_to_delete = Chapter.objects.get(pk=chapter_id)
        elements_to_delete = Element.objects.filter(chapter=chapter_to_delete)
        check_and_delete_files(elements_to_delete)  ## Image files deletion
        print('Chapter was DELETED')
        chapter_to_delete.delete()
    except ObjectDoesNotExist:
        print( "Chapter doesn't exist.")

    return HttpResponseRedirect(('/man_contents'))

# # =========== DELETE SUBSECTION  =======================
@staff_member_required
def section_delete(request, section_id):
    """ Delete section instance
        Delete all appropriate subsection and element instances
    """
    try:
        section_to_delete = Section.objects.get(pk=section_id)
        elements_to_delete = Element.objects.filter(section=section_to_delete)
        check_and_delete_files(elements_to_delete)  ## Image files deletion
        print('Section was DELETED')
        section_to_delete.delete()
    except ObjectDoesNotExist:
        print( "Section doesn't exist.")

    return HttpResponseRedirect(('/man_contents'))

# # =========== DELETE SUBSECTION  =======================
@staff_member_required
def subsection_delete(request, subsection_id):
    """ Delete subsection instance
        Delete all appropriate element instances
    """
    try:
        subsection_to_delete = Subsection.objects.get(pk=subsection_id)
        elements_to_delete = Element.objects.filter(subsection=subsection_to_delete)
        check_and_delete_files(elements_to_delete)  ## Image files deletion
        print('Subsection was DELETED')
        subsection_to_delete.delete()
    except ObjectDoesNotExist:
        print( "Subsection doesn't exist.")

    return HttpResponseRedirect(('/man_contents'))

# # =========== DELETE ELEMENT  =======================
@staff_member_required
def element_delete(request, element_id):
    """ Delete element instance
        Delete appropriate image file
    """
    try:
        element_to_delete = Element.objects.filter(pk=element_id).first()
        subsection_id = element_to_delete.subsection.id
        print('subsection_id = ', subsection_id)
        print('element_id = ', element_id)
        try:
            print('begin to try delete = ', element_id)

            check_and_delete_files(element_to_delete)  ## Image files deletion
        except Exception:
            print( "Can't delete file or file doesn't exist.")
        print('right befor element deletion')
        print('Element was DELETED')
        element_to_delete.delete()
    except ObjectDoesNotExist:
        print( "Subsection doesn't exist.")

    return HttpResponseRedirect(('/subsection_edit/'+str(subsection_id)))




# ====================================================================
def delete_file(filename):
    """ Delete image file while deleted the associated database instance """
    try:
        os.remove(filename)
    except Exception as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
        print ('Exception raised while deleting image file.')


from collections import Iterable

def check_and_delete_files(elements_to_delete):
    """ Checks and delete element image file, 
        while deleting the corresponding instance
    """
    if isinstance(elements_to_delete, Iterable):
        print('WOW - iterable')
        for element in elements_to_delete:
            if element.image.name:
                fname_element_to_delete = (settings.MEDIA_ROOT +
                                            element.image.name)
                if os.path.isfile(fname_element_to_delete):
                    delete_file(fname_element_to_delete)
                    print('IMAGE FILE DELETED SUCCESSFULLY', 
                          fname_element_to_delete)
                else:
                    print('ELEMENT IMAGE FILE DOES NOT EXIST', 
                          fname_element_to_delete)
            else:
                print('NOTHING - NO series image in database to delete')

    else:
        print('Print - sorry - not iterable')
        element = elements_to_delete

        if element.image.name:
            fname_element_to_delete = (settings.MEDIA_ROOT +
                                        element.image.name)
            if os.path.isfile(fname_element_to_delete):
                delete_file(fname_element_to_delete)
                print('IMAGE FILE DELETED SUCCESSFULLY', 
                      fname_element_to_delete)
            else:
                print('SERIES IMAGE FILE DOES NOT EXIST', 
                      fname_element_to_delete)
        else:
            print('NOTHING - NO series image in database to delete')




# # =========== ADD CHAPTER (FBV) -  add only the chapter itself =========
@staff_member_required
def chapter_add(request):
    ''' Сreate a brand new Chapter object
    '''    
    chapter = Chapter()
    chapter_form = ChapterForm()

    if request.method == "POST":
        chapter_form = ChapterForm(request.POST)

        if chapter_form.is_valid():
            created_chapter = chapter_form.save(commit=True)
            print('chapter_form is valid and saved')
            
            return HttpResponseRedirect(created_chapter.get_absolute_url())
  
    # else:
    chapter_form = ChapterForm(initial=({
                    'name': 'initial_name',
                }))

    return render(request, "chapter_add_edit.html", {
        "chapter_form": chapter_form,
        "page_name": "Создание главы",
        'is_editor': request.user.is_active and
                     request.user.is_staff,
    })


# # =========== ADD SECTION (FBV) -  add only the subsection itself ========
@staff_member_required
def section_add(request, chapter_id):
    ''' Сreate a brand new Section object only
        Foreign keys are being specially inserted in the view
    '''    
    chapter = Chapter.objects.get(pk=chapter_id)
    print('BEGINS', chapter.id, chapter)

    section = Section()
    section_form = SectionForm()

    if request.method == "POST":
        section_form = SectionForm(request.POST)

        if section_form.is_valid():
            created_section = section_form.save(commit=False)
            created_section.chapter_id = chapter.id
            created_section.save()            
            print('section_form is valid and saved')
            
            return HttpResponseRedirect(created_section.get_absolute_url())
  
    # else:
    section_form = SectionForm(initial=({
                    'name': 'initial_name',
                    'chapter': chapter,
                }))

    return render(request, "section_add_edit.html", {
        "section_form": section_form,
        "chapter": chapter,
        "page_name": "Создание раздела",
        'is_editor': request.user.is_active and
                     request.user.is_staff,
    })


# # =========== ADD SUBSECTION (FBV) -  add only the subsection itself ======
@staff_member_required
def subsection_add(request, section_id):
    ''' Сreate a brand new Subsection object only (no elements)
        with further redirection to the elements creation/editign
        Foreign keys are being specially inserted in the view
    '''    
    section = Section.objects.get(pk=section_id)
    chapter = section.chapter
    print('BEGINS', chapter.id, chapter, section.id, section)

    subsection = Subsection()
    subsection_form = PartialSubsectionForm()

    if request.method == "POST":
        subsection_form = PartialSubsectionForm(request.POST)

        if subsection_form.is_valid():
            created_subsection = subsection_form.save(commit=False)
            created_subsection.chapter_id = chapter.id
            created_subsection.section_id = section.id
            created_subsection.save()            
            print('subsection_form is valid and saved')
            
            return HttpResponseRedirect(created_subsection.get_absolute_url())  

    # else:
    subsection_form = PartialSubsectionForm(initial=({
                    'name': 'initial_name',
                    'chapter': chapter,
                    'section': section,
                }))

    return render(request, "subsection_add.html", {
        "subsection_form": subsection_form,
        "chapter": chapter,
        "section": section,
    })




# # =========== EDIT CHAPTER (functionBased) =======================
@staff_member_required
def chapter_edit(request, chapter_id):
    ''' Edit a Chapter object  
    '''
    chapter = Chapter.objects.get(pk=chapter_id) 
    chapter_form = ChapterForm(instance=chapter)

    if request.method == "POST":
        chapter_form = ChapterForm(request.POST, instance=chapter)

        if chapter_form.is_valid():
            print('chapter_form is valid')
            created_chapter = chapter_form.save(commit=True)

            return HttpResponseRedirect(created_chapter.get_absolute_url())

    return render(request, "chapter_add_edit.html", {
        "chapter_id": chapter_id, 
        "chapter_form": chapter_form,
        "page_name": "Редактирование главы",
        'is_editor': request.user.is_active and
                     request.user.is_staff,
    })



# # =========== EDIT SECTION (functionBased) =======================
@staff_member_required
def section_edit(request, section_id):
    ''' Edit a Section object  
        Foreign keys are being inserted especially in the view
    '''
    section = Section.objects.get(pk=section_id) 
    chapter = section.chapter

    section_form = SectionForm(instance=section) 

    if request.method == "POST":
        section_form = SectionForm(request.POST, instance=section)

        if section_form.is_valid():
            print('section_form is valid')
            created_section = section_form.save(commit=False)
            created_section.chapter_id = chapter.id
            created_section.section_id = section.id
            created_section.save()

        return HttpResponseRedirect(created_section.get_absolute_url())

    return render(request, "section_add_edit.html", {
        "section_form": section_form,
        "chapter": chapter,
        "section_id": section_id, 
        "page_name": "Редактирование раздела",
        'is_editor': request.user.is_active and
                     request.user.is_staff,
    })


# # =========== EDIT SUBSECTION (functionBased) =======================
@staff_member_required
def subsection_edit(request, subsection_id):
    ''' Edit a Subsection object  
        and its related Element objects using inlineformset_factory.
        Foreign keys are being inserted especially in the view
    '''
    subsection = Subsection.objects.get(pk=subsection_id)  
    chapter = subsection.chapter
    section = subsection.section

    subsection_form = PartialSubsectionForm(instance=subsection) 
    ElementInlineFormSet = inlineformset_factory(Subsection, Element, 
                    extra=1, 
                    exclude=['chapter', 'section'], 
                    can_delete=True,
                    widgets={'text': Textarea(attrs={'cols': 50, 'rows': 5}),
                })

    formset=ElementInlineFormSet(instance=subsection, 
             queryset=subsection.element.order_by("row", "column", "position"))

    if request.method == "POST":
        subsection_form = PartialSubsectionForm(request.POST or None, 
                                                request.FILES or None,
                                                instance=subsection)

        if subsection_form.is_valid():
            print('subsection_form is valid')
            created_subsection = subsection_form.save(commit=False)
            created_subsection.chapter_id = chapter.id
            created_subsection.section_id = section.id
            created_subsection.save()

            formset = ElementInlineFormSet(request.POST or None, 
                                            request.FILES or None, 
                                            instance=created_subsection)

            if formset.is_valid():
                print('formset is valid')
            else:
                print ('formset is invalid')
            
            print ('wwwwwwwwwww ___ formset_length = ', len(formset))


            for element_form in formset:
                # print('wwwwwwwwwwwwwwwwww   is element_form valid?')
                if element_form.is_valid():
                    print('element_form is valid')
                    obj = element_form.save(commit=False)
                    # print ('wwwwwwwwwww ___ element_form.text = ', obj.text)
                    if obj.text:                        
                        obj.chapter_id = chapter.id
                        obj.section_id = section.id
                        obj.save()

                else:
                    print('element_form is INvalid')
            

            return HttpResponseRedirect(created_subsection.get_absolute_url())


    return render(request, "subsection_edit.html", {
        "chapter": chapter,
        "section": section,
        "subsection_id": subsection_id, 
        "subsection_form": subsection_form,
        "formset": formset,
        'is_editor': request.user.is_active and
                     request.user.is_staff,
        "tohide": "hidden",
        "fullsize": "col-md-12 col-sm-12 col-xs-12"
    })


# # =========== EDIT RIGHT GROUP (functionBased) ... under development... ==
@staff_member_required
def right_group_creedit(request):
    ''' Create and edit a RightColElement group objects  
    '''
    # subsection_form = PartialSubsectionForm(instance=subsection) 
    right_group = PartialSubsectionForm(instance=subsection) 
    ElementInlineFormSet = inlineformset_factory(Subsection, Element, 
                    extra=1, 
                    exclude=['chapter', 'section'], 
                    can_delete=True,
                    widgets={'text': Textarea(attrs={'cols': 50, 'rows': 5}),
                })

    formset=ElementInlineFormSet(instance=subsection, 
             queryset=subsection.element.order_by("row", "column", "position"))

    if request.method == "POST":
        # subsection_form = PartialSubsectionForm(request.POST or None, 
        #                                         request.FILES or None,
        #                                         instance=subsection)
        subsection_form = PartialSubsectionForm(request.POST or None, 
                                                request.FILES or None,
                                                instance=subsection)

        if subsection_form.is_valid():
            print('subsection_form is valid')
            created_subsection = subsection_form.save(commit=False)
            created_subsection.chapter_id = chapter.id
            created_subsection.section_id = section.id
            created_subsection.save()

            formset = ElementInlineFormSet(request.POST or None, 
                                            request.FILES or None, 
                                            instance=created_subsection)

            if formset.is_valid():
                print('formset is valid')
            else:
                print ('formset is invalid')
            
            print ('wwwwwwwwwww ___ formset_length = ', len(formset))


            for element_form in formset:
                # print('wwwwwwwwwwwwwwwwww   is element_form valid?')
                if element_form.is_valid():
                    print('element_form is valid')
                    obj = element_form.save(commit=False)
                    # print ('wwwwwwwwwww ___ element_form.text = ', obj.text)
                    if obj.text:                        
                        obj.chapter_id = chapter.id
                        obj.section_id = section.id
                        obj.save()

                else:
                    print('element_form is INvalid')
            

            return HttpResponseRedirect(created_subsection.get_absolute_url())


    return render(request, "right_group_creedit.html", {
        "formset": formset,
        'is_editor': request.user.is_active and
                     request.user.is_staff,
        "tohide": "hidden",
    })




# ==========================================


  












